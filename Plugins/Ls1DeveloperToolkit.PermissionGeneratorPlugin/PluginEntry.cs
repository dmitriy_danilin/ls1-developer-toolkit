﻿using System.Windows.Forms;
using Ls1DeveloperToolkit.PermissionGeneratorPlugin.Views;
using Ninject;
using Ninject.Modules;

namespace Ls1DeveloperToolkit.PermissionGeneratorPlugin
{
    public class PluginNinjectModule : NinjectModule
    {
        /// <summary>Loads the module into the kernel.</summary>
        public override void Load()
        {
        }
    }

    public class PluginEntry : BasePlugin.BasePlugin
    {
        private UserControl pluginPage;

        public override string PluginVisibleName => "Permission generator";

        public override UserControl PluginPage => pluginPage ?? (pluginPage = new PermissionGeneratorPage(new StandardKernel(new PluginNinjectModule())));
    }
}
