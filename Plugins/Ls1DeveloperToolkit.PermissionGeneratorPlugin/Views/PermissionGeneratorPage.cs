﻿using Ninject;
using System;
using System.Globalization;
using System.Windows.Forms;

namespace Ls1DeveloperToolkit.PermissionGeneratorPlugin.Views
{
    public partial class PermissionGeneratorPage : UserControl
    {
        private readonly IKernel kernel;

        public PermissionGeneratorPage(IKernel kernel)
        {
            InitializeComponent();

            this.kernel = kernel;
        }
        public PermissionGeneratorPage()
        {
            InitializeComponent();
        }

        private void btnAdd_Click(object sender, System.EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbCaption.Text) && cmbGroup.SelectedIndex != -1)
            {
                Guid codeGuid = Guid.NewGuid();
                Guid localisationGuid = Guid.NewGuid();

                if (!string.IsNullOrEmpty(tbScript.Text))
                    tbScript.AppendText("\n");

                tbScript.AppendText($"exec spSECURITY_AddPermission_1_0 @dataAreaID, '{localisationGuid}', '{tbCaption.Text}', @{cmbGroup.Text}, 'XXXXX', '{codeGuid}', 0");

                tbCode.AppendText($"public static string {CultureInfo.InvariantCulture.TextInfo.ToTitleCase(tbCaption.Text).Replace(" ", string.Empty)} {{ get {{ return \"{codeGuid}\"; }} }}\n");

                tbCaption.Text = string.Empty;
            }
        }
    }
}
