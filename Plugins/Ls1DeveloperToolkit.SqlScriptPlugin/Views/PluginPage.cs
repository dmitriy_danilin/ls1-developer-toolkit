﻿using System;
using System.Windows.Forms;
using System.Text.RegularExpressions;

using Ninject;

using Ls1DeveloperToolkit.SqlScriptPlugin.Interfaces;

namespace Ls1DeveloperToolkit.SqlScriptPlugin.Views
{
    public partial class PluginPage : UserControl
    {
        private readonly IKernel kernel;

        public PluginPage(IKernel kernel)
        {
            InitializeComponent();

            this.kernel = kernel;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            InitializeScriptTemplatesList();
        }

        private void InitializeScriptTemplatesList()
        {
            var scriptTemplateService = kernel.Get<IScriptTemplateService>();
            var scriptTemplates = scriptTemplateService.GetScriptTemplatesList();

            foreach (var scriptTemplate in scriptTemplates)
            {
                Match match = Regex.Match(scriptTemplate, @"([A-Z])\w+.sql");

                ListViewItem listViewItem = new ListViewItem
                {
                    Text = match.Captures[0]?.Value ?? scriptTemplate,
                    Tag = scriptTemplate
                };

                scriptTemplateListView.Items.Add(listViewItem);
            }
        }

        private void scriptTemplateListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (scriptTemplateListView.SelectedItems.Count <= 0)
                return;

            PrintSciptTemplateContent(scriptTemplateListView.SelectedItems[0].Tag.ToString());
        }

        private void PrintSciptTemplateContent(string scriptTemplateResourceName)
        {
            var scriptTemplateService = kernel.Get<IScriptTemplateService>();
            scriptTemplateContent.Text = scriptTemplateService.GetScriptTemplateConent(scriptTemplateResourceName);
        }
    }
}
