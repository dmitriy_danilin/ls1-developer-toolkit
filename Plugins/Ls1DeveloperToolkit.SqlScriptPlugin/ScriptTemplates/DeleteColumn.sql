﻿IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'tablename' AND COLUMN_NAME='columnname')
BEGIN
	ALTER TABLE [dbo].[tablename] DROP CONSTRAINT [tablename_DFV_columnname]
	ALTER TABLE [dbo].[tablename] DROP COLUMN [columnname]
END
GO