﻿IF EXISTS (SELECT * FROM sys.objects WHERE  object_id = OBJECT_ID(N'functionname') AND type IN ( N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
    DROP FUNCTION functionname
END
GO

CREATE FUNCTION functionname(@param AS type)
RETURNS @Result TABLE (COLUMN1 type,
					   COLUMN2 type)
AS 
BEGIN
	IF @param = '' OR @param IS NULL
		INSERT INTO @Result
		SELECT * FROM TABLE1
	ELSE
		INSERT INTO @Result 
		SELECT * FROM TABLE1
	RETURN
END
GO