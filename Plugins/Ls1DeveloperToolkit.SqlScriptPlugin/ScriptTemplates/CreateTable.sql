﻿IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'tablename')
BEGIN
	CREATE TABLE [dbo].[tablename]
	(
		[columnname0] [bigint] IDENTITY(1,1) NOT NULL,
		[DATAAREAID] [nvarchar](4) NOT NULL CONSTRAINT [tablename_DFV_DATAAREAID]  DEFAULT ('LSR'),
		[columnname1] [nvarchar](20) NULL,
		[columnname2] [nvarchar](20) NULL,
		[columnname3] [int] NOT NULL,
		[columnname4] [datetime] NOT NULL CONSTRAINT [tablename_DFV_columnname4]  DEFAULT (getdate()),
		[columnname5] [nvarchar](100) NULL,
		[columnname6] [nvarchar](100) NULL,
		[columnname7] [nvarchar](1024) NULL,
		[columnname8] [ntext] NULL,
		CONSTRAINT [PK_tablename] PRIMARY KEY CLUSTERED 
		(
			[columnname0] ASC,
			[DATAAREAID] ASC
		),
	) ON [PRIMARY]
END
GO