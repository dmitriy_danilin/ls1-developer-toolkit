﻿IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'schema' AND TABLE_NAME = 'tablename')
BEGIN
	EXEC sp_rename 'schema.tablename', 'newtablename'
END