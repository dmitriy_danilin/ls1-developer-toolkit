﻿namespace Ls1DeveloperToolkit.DataProviderCreatorPlugin.Views
{
    partial class PluginPage
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtBusinessObjectName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNameSpace = new System.Windows.Forms.TextBox();
            this.chkbxDataAreaId = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtColumnsCount = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnAddColumn = new System.Windows.Forms.Button();
            this.pnlColumns = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chkbDeleteAll = new System.Windows.Forms.CheckBox();
            this.chkbDelete = new System.Windows.Forms.CheckBox();
            this.chkbFind = new System.Windows.Forms.CheckBox();
            this.chkbSave = new System.Windows.Forms.CheckBox();
            this.chkbExists = new System.Windows.Forms.CheckBox();
            this.chkbGetList = new System.Windows.Forms.CheckBox();
            this.chkbGet = new System.Windows.Forms.CheckBox();
            this.tabctrlResult = new System.Windows.Forms.TabControl();
            this.tabBusinessObject = new System.Windows.Forms.TabPage();
            this.txtBusinessObjectCode = new System.Windows.Forms.TextBox();
            this.tabDataProvider = new System.Windows.Forms.TabPage();
            this.txtDataProviderCode = new System.Windows.Forms.TextBox();
            this.tabSqlDataProvider = new System.Windows.Forms.TabPage();
            this.txtSqlDataProviderCode = new System.Windows.Forms.TextBox();
            this.tabPoresyProvider = new System.Windows.Forms.TabPage();
            this.txtPoresyProviderCode = new System.Windows.Forms.TextBox();
            this.tabSqlPoresyProvider = new System.Windows.Forms.TabPage();
            this.txtSqlPoresyProviderCode = new System.Windows.Forms.TextBox();
            this.tabSqlScript = new System.Windows.Forms.TabPage();
            this.txtSqlScript = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtTableName = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabctrlResult.SuspendLayout();
            this.tabBusinessObject.SuspendLayout();
            this.tabDataProvider.SuspendLayout();
            this.tabSqlDataProvider.SuspendLayout();
            this.tabPoresyProvider.SuspendLayout();
            this.tabSqlPoresyProvider.SuspendLayout();
            this.tabSqlScript.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtBusinessObjectName
            // 
            this.txtBusinessObjectName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBusinessObjectName.Location = new System.Drawing.Point(533, 3);
            this.txtBusinessObjectName.Name = "txtBusinessObjectName";
            this.txtBusinessObjectName.Size = new System.Drawing.Size(290, 20);
            this.txtBusinessObjectName.TabIndex = 2;
            this.txtBusinessObjectName.TextChanged += new System.EventHandler(this.OnChanged);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(443, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Business object:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(33, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Namespace:";
            // 
            // txtNameSpace
            // 
            this.txtNameSpace.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNameSpace.Location = new System.Drawing.Point(106, 49);
            this.txtNameSpace.Name = "txtNameSpace";
            this.txtNameSpace.Size = new System.Drawing.Size(717, 20);
            this.txtNameSpace.TabIndex = 3;
            this.txtNameSpace.TextChanged += new System.EventHandler(this.OnChanged);
            // 
            // chkbxDataAreaId
            // 
            this.chkbxDataAreaId.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkbxDataAreaId.AutoSize = true;
            this.chkbxDataAreaId.Checked = true;
            this.chkbxDataAreaId.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkbxDataAreaId.Location = new System.Drawing.Point(833, 52);
            this.chkbxDataAreaId.Name = "chkbxDataAreaId";
            this.chkbxDataAreaId.Size = new System.Drawing.Size(102, 17);
            this.chkbxDataAreaId.TabIndex = 4;
            this.chkbxDataAreaId.Text = "Use DataAreaId";
            this.chkbxDataAreaId.UseVisualStyleBackColor = true;
            this.chkbxDataAreaId.CheckedChanged += new System.EventHandler(this.OnChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.txtColumnsCount);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.btnAddColumn);
            this.groupBox1.Controls.Add(this.pnlColumns);
            this.groupBox1.Location = new System.Drawing.Point(3, 85);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(820, 232);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Columns";
            // 
            // txtColumnsCount
            // 
            this.txtColumnsCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtColumnsCount.Location = new System.Drawing.Point(725, 205);
            this.txtColumnsCount.Name = "txtColumnsCount";
            this.txtColumnsCount.ReadOnly = true;
            this.txtColumnsCount.Size = new System.Drawing.Size(59, 20);
            this.txtColumnsCount.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(639, 208);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Columns count:";
            // 
            // btnAddColumn
            // 
            this.btnAddColumn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddColumn.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnAddColumn.Location = new System.Drawing.Point(791, 203);
            this.btnAddColumn.Name = "btnAddColumn";
            this.btnAddColumn.Size = new System.Drawing.Size(23, 23);
            this.btnAddColumn.TabIndex = 12;
            this.btnAddColumn.Text = "+";
            this.btnAddColumn.UseVisualStyleBackColor = true;
            this.btnAddColumn.Click += new System.EventHandler(this.btnAddColumn_Click);
            // 
            // pnlColumns
            // 
            this.pnlColumns.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlColumns.AutoScroll = true;
            this.pnlColumns.Location = new System.Drawing.Point(7, 20);
            this.pnlColumns.Name = "pnlColumns";
            this.pnlColumns.Size = new System.Drawing.Size(807, 180);
            this.pnlColumns.TabIndex = 13;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.chkbDeleteAll);
            this.groupBox2.Controls.Add(this.chkbDelete);
            this.groupBox2.Controls.Add(this.chkbFind);
            this.groupBox2.Controls.Add(this.chkbSave);
            this.groupBox2.Controls.Add(this.chkbExists);
            this.groupBox2.Controls.Add(this.chkbGetList);
            this.groupBox2.Controls.Add(this.chkbGet);
            this.groupBox2.Location = new System.Drawing.Point(833, 85);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(98, 189);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Methods";
            // 
            // chkbDeleteAll
            // 
            this.chkbDeleteAll.AutoSize = true;
            this.chkbDeleteAll.Location = new System.Drawing.Point(7, 158);
            this.chkbDeleteAll.Name = "chkbDeleteAll";
            this.chkbDeleteAll.Size = new System.Drawing.Size(68, 17);
            this.chkbDeleteAll.TabIndex = 11;
            this.chkbDeleteAll.Text = "DeleteAll";
            this.chkbDeleteAll.UseVisualStyleBackColor = true;
            this.chkbDeleteAll.CheckedChanged += new System.EventHandler(this.OnChanged);
            // 
            // chkbDelete
            // 
            this.chkbDelete.AutoSize = true;
            this.chkbDelete.Location = new System.Drawing.Point(7, 135);
            this.chkbDelete.Name = "chkbDelete";
            this.chkbDelete.Size = new System.Drawing.Size(57, 17);
            this.chkbDelete.TabIndex = 10;
            this.chkbDelete.Text = "Delete";
            this.chkbDelete.UseVisualStyleBackColor = true;
            this.chkbDelete.CheckedChanged += new System.EventHandler(this.OnChanged);
            // 
            // chkbFind
            // 
            this.chkbFind.AutoSize = true;
            this.chkbFind.Location = new System.Drawing.Point(7, 112);
            this.chkbFind.Name = "chkbFind";
            this.chkbFind.Size = new System.Drawing.Size(46, 17);
            this.chkbFind.TabIndex = 9;
            this.chkbFind.Text = "Find";
            this.chkbFind.UseVisualStyleBackColor = true;
            this.chkbFind.CheckedChanged += new System.EventHandler(this.OnChanged);
            // 
            // chkbSave
            // 
            this.chkbSave.AutoSize = true;
            this.chkbSave.Checked = true;
            this.chkbSave.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkbSave.Location = new System.Drawing.Point(7, 89);
            this.chkbSave.Name = "chkbSave";
            this.chkbSave.Size = new System.Drawing.Size(51, 17);
            this.chkbSave.TabIndex = 8;
            this.chkbSave.Text = "Save";
            this.chkbSave.UseVisualStyleBackColor = true;
            this.chkbSave.CheckedChanged += new System.EventHandler(this.OnChanged);
            // 
            // chkbExists
            // 
            this.chkbExists.AutoSize = true;
            this.chkbExists.Checked = true;
            this.chkbExists.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkbExists.Location = new System.Drawing.Point(7, 66);
            this.chkbExists.Name = "chkbExists";
            this.chkbExists.Size = new System.Drawing.Size(53, 17);
            this.chkbExists.TabIndex = 7;
            this.chkbExists.Text = "Exists";
            this.chkbExists.UseVisualStyleBackColor = true;
            this.chkbExists.CheckedChanged += new System.EventHandler(this.OnChanged);
            // 
            // chkbGetList
            // 
            this.chkbGetList.AutoSize = true;
            this.chkbGetList.Checked = true;
            this.chkbGetList.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkbGetList.Location = new System.Drawing.Point(7, 43);
            this.chkbGetList.Name = "chkbGetList";
            this.chkbGetList.Size = new System.Drawing.Size(59, 17);
            this.chkbGetList.TabIndex = 6;
            this.chkbGetList.Text = "GetList";
            this.chkbGetList.UseVisualStyleBackColor = true;
            this.chkbGetList.CheckedChanged += new System.EventHandler(this.OnChanged);
            // 
            // chkbGet
            // 
            this.chkbGet.AutoSize = true;
            this.chkbGet.Checked = true;
            this.chkbGet.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkbGet.Location = new System.Drawing.Point(7, 20);
            this.chkbGet.Name = "chkbGet";
            this.chkbGet.Size = new System.Drawing.Size(43, 17);
            this.chkbGet.TabIndex = 5;
            this.chkbGet.Text = "Get";
            this.chkbGet.UseVisualStyleBackColor = true;
            this.chkbGet.CheckedChanged += new System.EventHandler(this.OnChanged);
            // 
            // tabctrlResult
            // 
            this.tabctrlResult.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabctrlResult.Controls.Add(this.tabBusinessObject);
            this.tabctrlResult.Controls.Add(this.tabDataProvider);
            this.tabctrlResult.Controls.Add(this.tabSqlDataProvider);
            this.tabctrlResult.Controls.Add(this.tabPoresyProvider);
            this.tabctrlResult.Controls.Add(this.tabSqlPoresyProvider);
            this.tabctrlResult.Controls.Add(this.tabSqlScript);
            this.tabctrlResult.Location = new System.Drawing.Point(3, 323);
            this.tabctrlResult.Name = "tabctrlResult";
            this.tabctrlResult.SelectedIndex = 0;
            this.tabctrlResult.Size = new System.Drawing.Size(928, 275);
            this.tabctrlResult.TabIndex = 14;
            // 
            // tabBusinessObject
            // 
            this.tabBusinessObject.Controls.Add(this.txtBusinessObjectCode);
            this.tabBusinessObject.Location = new System.Drawing.Point(4, 22);
            this.tabBusinessObject.Name = "tabBusinessObject";
            this.tabBusinessObject.Padding = new System.Windows.Forms.Padding(3);
            this.tabBusinessObject.Size = new System.Drawing.Size(920, 249);
            this.tabBusinessObject.TabIndex = 0;
            this.tabBusinessObject.Text = "BusinessObject";
            this.tabBusinessObject.UseVisualStyleBackColor = true;
            // 
            // txtBusinessObjectCode
            // 
            this.txtBusinessObjectCode.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBusinessObjectCode.Location = new System.Drawing.Point(3, 6);
            this.txtBusinessObjectCode.Multiline = true;
            this.txtBusinessObjectCode.Name = "txtBusinessObjectCode";
            this.txtBusinessObjectCode.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtBusinessObjectCode.Size = new System.Drawing.Size(911, 237);
            this.txtBusinessObjectCode.TabIndex = 0;
            // 
            // tabDataProvider
            // 
            this.tabDataProvider.Controls.Add(this.txtDataProviderCode);
            this.tabDataProvider.Location = new System.Drawing.Point(4, 22);
            this.tabDataProvider.Name = "tabDataProvider";
            this.tabDataProvider.Padding = new System.Windows.Forms.Padding(3);
            this.tabDataProvider.Size = new System.Drawing.Size(920, 249);
            this.tabDataProvider.TabIndex = 1;
            this.tabDataProvider.Text = "DataProvider";
            this.tabDataProvider.UseVisualStyleBackColor = true;
            // 
            // txtDataProviderCode
            // 
            this.txtDataProviderCode.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDataProviderCode.Location = new System.Drawing.Point(5, 6);
            this.txtDataProviderCode.Multiline = true;
            this.txtDataProviderCode.Name = "txtDataProviderCode";
            this.txtDataProviderCode.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtDataProviderCode.Size = new System.Drawing.Size(911, 237);
            this.txtDataProviderCode.TabIndex = 1;
            // 
            // tabSqlDataProvider
            // 
            this.tabSqlDataProvider.Controls.Add(this.txtSqlDataProviderCode);
            this.tabSqlDataProvider.Location = new System.Drawing.Point(4, 22);
            this.tabSqlDataProvider.Name = "tabSqlDataProvider";
            this.tabSqlDataProvider.Size = new System.Drawing.Size(920, 249);
            this.tabSqlDataProvider.TabIndex = 2;
            this.tabSqlDataProvider.Text = "SqlDataProvider";
            this.tabSqlDataProvider.UseVisualStyleBackColor = true;
            // 
            // txtSqlDataProviderCode
            // 
            this.txtSqlDataProviderCode.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSqlDataProviderCode.Location = new System.Drawing.Point(5, 6);
            this.txtSqlDataProviderCode.Multiline = true;
            this.txtSqlDataProviderCode.Name = "txtSqlDataProviderCode";
            this.txtSqlDataProviderCode.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtSqlDataProviderCode.Size = new System.Drawing.Size(911, 237);
            this.txtSqlDataProviderCode.TabIndex = 1;
            // 
            // tabPoresyProvider
            // 
            this.tabPoresyProvider.Controls.Add(this.txtPoresyProviderCode);
            this.tabPoresyProvider.Location = new System.Drawing.Point(4, 22);
            this.tabPoresyProvider.Name = "tabPoresyProvider";
            this.tabPoresyProvider.Size = new System.Drawing.Size(920, 249);
            this.tabPoresyProvider.TabIndex = 3;
            this.tabPoresyProvider.Text = "PoresyProvider";
            this.tabPoresyProvider.UseVisualStyleBackColor = true;
            // 
            // txtPoresyProviderCode
            // 
            this.txtPoresyProviderCode.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPoresyProviderCode.Location = new System.Drawing.Point(5, 6);
            this.txtPoresyProviderCode.Multiline = true;
            this.txtPoresyProviderCode.Name = "txtPoresyProviderCode";
            this.txtPoresyProviderCode.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtPoresyProviderCode.Size = new System.Drawing.Size(911, 237);
            this.txtPoresyProviderCode.TabIndex = 1;
            // 
            // tabSqlPoresyProvider
            // 
            this.tabSqlPoresyProvider.Controls.Add(this.txtSqlPoresyProviderCode);
            this.tabSqlPoresyProvider.Location = new System.Drawing.Point(4, 22);
            this.tabSqlPoresyProvider.Name = "tabSqlPoresyProvider";
            this.tabSqlPoresyProvider.Size = new System.Drawing.Size(920, 249);
            this.tabSqlPoresyProvider.TabIndex = 4;
            this.tabSqlPoresyProvider.Text = "SQL_PoresyProvider";
            this.tabSqlPoresyProvider.UseVisualStyleBackColor = true;
            // 
            // txtSqlPoresyProviderCode
            // 
            this.txtSqlPoresyProviderCode.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSqlPoresyProviderCode.Location = new System.Drawing.Point(5, 6);
            this.txtSqlPoresyProviderCode.Multiline = true;
            this.txtSqlPoresyProviderCode.Name = "txtSqlPoresyProviderCode";
            this.txtSqlPoresyProviderCode.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtSqlPoresyProviderCode.Size = new System.Drawing.Size(911, 237);
            this.txtSqlPoresyProviderCode.TabIndex = 1;
            // 
            // tabSqlScript
            // 
            this.tabSqlScript.Controls.Add(this.txtSqlScript);
            this.tabSqlScript.Location = new System.Drawing.Point(4, 22);
            this.tabSqlScript.Name = "tabSqlScript";
            this.tabSqlScript.Size = new System.Drawing.Size(920, 249);
            this.tabSqlScript.TabIndex = 5;
            this.tabSqlScript.Text = "SQL Script";
            this.tabSqlScript.UseVisualStyleBackColor = true;
            // 
            // txtSqlScript
            // 
            this.txtSqlScript.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSqlScript.Location = new System.Drawing.Point(5, 6);
            this.txtSqlScript.Multiline = true;
            this.txtSqlScript.Name = "txtSqlScript";
            this.txtSqlScript.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtSqlScript.Size = new System.Drawing.Size(911, 237);
            this.txtSqlScript.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(35, 6);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Table name:";
            // 
            // txtTableName
            // 
            this.txtTableName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTableName.Location = new System.Drawing.Point(106, 3);
            this.txtTableName.Name = "txtTableName";
            this.txtTableName.Size = new System.Drawing.Size(299, 20);
            this.txtTableName.TabIndex = 1;
            this.txtTableName.TextChanged += new System.EventHandler(this.OnChanged);
            this.txtTableName.Leave += new System.EventHandler(this.txtTableName_Leave);
            // 
            // PluginPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtTableName);
            this.Controls.Add(this.tabctrlResult);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.chkbxDataAreaId);
            this.Controls.Add(this.txtNameSpace);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtBusinessObjectName);
            this.Name = "PluginPage";
            this.Size = new System.Drawing.Size(945, 601);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabctrlResult.ResumeLayout(false);
            this.tabBusinessObject.ResumeLayout(false);
            this.tabBusinessObject.PerformLayout();
            this.tabDataProvider.ResumeLayout(false);
            this.tabDataProvider.PerformLayout();
            this.tabSqlDataProvider.ResumeLayout(false);
            this.tabSqlDataProvider.PerformLayout();
            this.tabPoresyProvider.ResumeLayout(false);
            this.tabPoresyProvider.PerformLayout();
            this.tabSqlPoresyProvider.ResumeLayout(false);
            this.tabSqlPoresyProvider.PerformLayout();
            this.tabSqlScript.ResumeLayout(false);
            this.tabSqlScript.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtBusinessObjectName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtNameSpace;
        private System.Windows.Forms.CheckBox chkbxDataAreaId;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel pnlColumns;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox chkbDeleteAll;
        private System.Windows.Forms.CheckBox chkbDelete;
        private System.Windows.Forms.CheckBox chkbFind;
        private System.Windows.Forms.CheckBox chkbSave;
        private System.Windows.Forms.CheckBox chkbExists;
        private System.Windows.Forms.CheckBox chkbGetList;
        private System.Windows.Forms.CheckBox chkbGet;
        private System.Windows.Forms.TextBox txtColumnsCount;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnAddColumn;
        private System.Windows.Forms.TabControl tabctrlResult;
        private System.Windows.Forms.TabPage tabBusinessObject;
        private System.Windows.Forms.TabPage tabDataProvider;
        private System.Windows.Forms.TabPage tabSqlDataProvider;
        private System.Windows.Forms.TabPage tabPoresyProvider;
        private System.Windows.Forms.TabPage tabSqlPoresyProvider;
        private System.Windows.Forms.TextBox txtBusinessObjectCode;
        private System.Windows.Forms.TextBox txtDataProviderCode;
        private System.Windows.Forms.TextBox txtSqlDataProviderCode;
        private System.Windows.Forms.TextBox txtPoresyProviderCode;
        private System.Windows.Forms.TextBox txtSqlPoresyProviderCode;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtTableName;
        private System.Windows.Forms.TabPage tabSqlScript;
        private System.Windows.Forms.TextBox txtSqlScript;
    }
}
