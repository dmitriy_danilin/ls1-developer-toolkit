﻿using System.Linq;
using System.Windows.Forms;
using static System.String;

using Ninject;

using Ls1DeveloperToolkit.DataProviderCreatorPlugin.UserControls;
using Ls1DeveloperToolkit.DataProviderCreatorPlugin.CodeGenerators;

namespace Ls1DeveloperToolkit.DataProviderCreatorPlugin.Views
{
    public partial class PluginPage : UserControl
    {
        private readonly IKernel kernel;

        private DataProviderInfo DataProviderInfo
        {
            get
            {
                return new DataProviderInfo()
                {
                    BusinessObject = txtBusinessObjectName.Text,
                    TableName = txtTableName.Text,
                    Columns = pnlColumns.Controls.OfType<ColumnControl>().Select(c => 
                        new DataProviderInfo.Column(
                            isPK: c.IsPrimaryKey,
                            colName: c.SqlColumnName,
                            sqlType: c.SqlType.ToUpper(),
                            propName: c.PropertyName, 
                            propType: c.PropertyType
                        )),
                    IsNeedMethodDelete = chkbDelete.Checked,
                    IsNeedMethodDeleteAll = chkbDeleteAll.Checked,
                    IsNeedMethodExists = chkbExists.Checked,
                    IsNeedMethodFind = chkbFind.Checked,
                    IsNeedMethodGet = chkbGet.Checked,
                    IsNeedMethodGetList = chkbGetList.Checked,
                    IsNeedMethodSave = chkbSave.Checked,
                    NameSpase = txtNameSpace.Text,
                    UseDataAreaId = chkbxDataAreaId.Checked
                };
            }
        }

        public PluginPage(IKernel kernel)
        {
            InitializeComponent();                    
            this.kernel = kernel;

            AddColumn();
            UpdateControls();            
        }

        #region Form controls methods

        private void UpdateControls()
        {
            txtColumnsCount.Text = pnlColumns.Controls.Count.ToString();

            bool businessObjectNameEmpty = IsNullOrWhiteSpace(txtBusinessObjectName.Text);
            tabBusinessObject.Text = GetBusinessObjectTabName();
            tabDataProvider.Text = GetDataProviderTabName();
            tabSqlDataProvider.Text = GetSqlDataProviderTabName();
        }

        #region tab methods

        private string GetBusinessObjectTabName()
        {
            return IsNullOrWhiteSpace(txtBusinessObjectName.Text)
                ? Properties.Resources.BusinessObjectTabName
                : $"{txtBusinessObjectName.Text}.cs";
        }

        private string GetDataProviderTabName()
        {
            return IsNullOrWhiteSpace(txtBusinessObjectName.Text)
                ? Properties.Resources.DataProviderTabName
                : $"I{txtBusinessObjectName.Text}Data.cs";
        }

        private string GetSqlDataProviderTabName()
        {
            return IsNullOrWhiteSpace(txtBusinessObjectName.Text)
                ? Properties.Resources.SqlDataProviderTabName
                : $"{txtBusinessObjectName.Text}Data.cs";
        }

        #endregion

        #region Validation

        private bool ValidateColumns()
        {
            return pnlColumns.Controls.OfType<ColumnControl>().All(c => c.ValidateControls());
        }

        private bool ValidateControls()
        {
            return !IsNullOrWhiteSpace(txtBusinessObjectName.Text) &&
                   !IsNullOrWhiteSpace(txtNameSpace.Text) &&
                   !IsNullOrWhiteSpace(txtTableName.Text) &&
                   ValidateColumns();
        }

        #endregion

        #region Column methods

        private void AddColumn()
        {
            ColumnControl cc = new ColumnControl() {  Id = (pnlColumns.Controls.OfType<ColumnControl>().LastOrDefault()?.Id + 1) ?? 1 };
            cc.BtnDelete_Click += ColumnControl_BtnDelete_Click;
            cc.ControlValueChanged += OnChanged;
            pnlColumns.Controls.Add(cc);

            AlignColumnControls();
        }

        private void AlignColumnControls()
        {
            int controlHeight = new ColumnControl().Height;
            int i = 0;
            pnlColumns.Controls.OfType<ColumnControl>().ToList().ForEach(c => c.Location = new System.Drawing.Point(2, controlHeight * i++));
        }

        #endregion

        #endregion

        #region Event handlers

        private void ColumnControl_BtnDelete_Click(object sender, System.EventArgs e)
        {
            if (sender is ColumnControl)
            {
                pnlColumns.Controls.Remove((ColumnControl)sender);
                AlignColumnControls();
                OnChanged(sender, e);
            }
        }

        private void OnChanged(object sender, System.EventArgs e)
        {
            UpdateControls();

            if (!ValidateControls()) return;

            tabBusinessObject.FillCodeTextBox<BusinessObjectCodeGenerator>(DataProviderInfo);
            tabDataProvider.FillCodeTextBox<DataProviderCodeGenerator>(DataProviderInfo);
            tabSqlDataProvider.FillCodeTextBox<SqlDataProviderCodeGenerator>(DataProviderInfo);
            tabPoresyProvider.FillCodeTextBox<PoresyProviderCodeGenerator>(DataProviderInfo);
            tabSqlPoresyProvider.FillCodeTextBox<SqlPoresyProviderCodeGenerator>(DataProviderInfo);
            tabSqlScript.FillCodeTextBox<SqlScriptCodeGenerator>(DataProviderInfo);
        }       

        private void btnAddColumn_Click(object sender, System.EventArgs e)
        {
            AddColumn();
            OnChanged(sender, e);
        }

        private void txtTableName_Leave(object sender, System.EventArgs e)
        {
            if (txtTableName.Text.Length > 3 && IsNullOrWhiteSpace(txtBusinessObjectName.Text))
                txtBusinessObjectName.Text = txtTableName.Text.ToLower().Replace("prs_", "").FirstCharUpper();
        }

        #endregion
    }
}
