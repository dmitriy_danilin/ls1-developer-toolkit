﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ls1DeveloperToolkit.DataProviderCreatorPlugin
{
    public class DataProviderInfo
    {
        public string BusinessObject { get; set; }
        public string NameSpase { get; set; }
        public bool UseDataAreaId { get; set; }
        public bool IsNeedMethodGet { get; set; }
        public bool IsNeedMethodGetList { get; set; }
        public bool IsNeedMethodExists { get; set; }
        public bool IsNeedMethodSave { get; set; }
        public bool IsNeedMethodFind { get; set; }
        public bool IsNeedMethodDelete { get; set; }
        public bool IsNeedMethodDeleteAll { get; set; }
        public string TableNameWithBrackets => tableName.AddSquareBrackets();
        public string BusinessObjectField => BusinessObject.FirstCharLower();

        public string TableName
        {
            get => tableName;
            set => tableName = value.Trim().RemoveSquareBrackets().Trim();
        }

        private string tableName;
        //public IEnumerable<(bool isPK, string colName, string sqlType, string propName, string propType)> Columns { get; set; }

        public IEnumerable<Column> Columns { get; set; }

        public IEnumerable<Column> GetPkColumns()
        {
            return Columns.Where(c => c.IsPK);
        }

        public IEnumerable<Column> GetNotPkColumns()
        {
            return Columns.Where(c => !c.IsPK);
        }

        public class Column
        {
            public bool IsPK { get; }
            public string ColName{ get; }
            public string SqlType { get; }
            public string PropName { get; }
            public string PropType { get; }

            public string FieldName => PropName.FirstCharLower();
            public string ColNameWithBrackets => ColName.AddSquareBrackets();


            public Column(bool isPK, string colName, string sqlType, string propName, string propType)
            {
                IsPK = isPK;
                ColName = colName.Trim().RemoveSquareBrackets();
                SqlType = sqlType.Trim();
                PropName = propName.Trim();
                PropType = propType.Trim();
            }
        }
    }
}
