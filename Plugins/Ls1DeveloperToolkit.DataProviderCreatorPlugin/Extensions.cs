﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Ls1DeveloperToolkit.DataProviderCreatorPlugin.Interfaces;
using static System.String;

namespace Ls1DeveloperToolkit.DataProviderCreatorPlugin
{
    public static class Extensions
    {
        public static void FillCodeTextBox<T>(this TabPage tab, DataProviderInfo dataProviderInfo) where T : ICodeGenerator, new()
        {
            TextBox txtCode = tab.Controls.OfType<TextBox>().FirstOrDefault();

            if (txtCode != null)
            {
                T codeGenerator = new T();
                txtCode.Text = codeGenerator.GenerateCode(dataProviderInfo);
            }
        }

        public static string FirstCharUpper(this string str)
        {
            return IsNullOrWhiteSpace(str)
                ? Empty
                : str.Substring(0, 1).ToUpper() + (str.Length > 1 ? str.Substring(1) : "");
        }

        public static string FirstCharLower(this string str)
        {
            return IsNullOrWhiteSpace(str)
                ? Empty
                : str.Substring(0, 1).ToLower() + (str.Length > 1 ? str.Substring(1) : "");
        }

        public static string AddSquareBrackets(this string str)
        {
            return IsNullOrWhiteSpace(str)
                ? Empty
                : $"[{str.Trim(new[] { '[', ']' })}]";
        }

        public static string RemoveSquareBrackets(this string str)
        {
            return IsNullOrWhiteSpace(str)
                ? Empty
                : $"{str.Trim(new[] { '[', ']' })}";
        }
    }
}
