﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ls1DeveloperToolkit.DataProviderCreatorPlugin.Interfaces;

namespace Ls1DeveloperToolkit.DataProviderCreatorPlugin.CodeGenerators
{
    public class SqlPoresyProviderCodeGenerator : ICodeGenerator
    {
        public SqlPoresyProviderCodeGenerator() { }

        public string GenerateCode(DataProviderInfo dataProviderInfo)
        {
            return $@"DataProviderFactory.Instance.Register<I{dataProviderInfo.BusinessObject}Data, {dataProviderInfo.BusinessObject}Data, {dataProviderInfo.BusinessObject}>();";
        }

        public void SaveToFile(string path)
        {

        }
    }
}
