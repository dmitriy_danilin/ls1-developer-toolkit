﻿using Ls1DeveloperToolkit.DataProviderCreatorPlugin.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.String;

namespace Ls1DeveloperToolkit.DataProviderCreatorPlugin.CodeGenerators
{
    public class DataProviderCodeGenerator : ICodeGenerator
    {
        public DataProviderCodeGenerator()
        {
        }

        public string GenerateCode(DataProviderInfo dataProviderInfo)
        {
            return $@"using LSOne.DataLayer.DataProviders;
using LSOne.DataLayer.GenericConnector.Interfaces;
using LSOne.Poresy.Core.DataLayer.BusinessObjects.{dataProviderInfo.NameSpase};
using LSOne.Utilities.DataTypes;
using System.Collections.Generic;

namespace LSOne.Poresy.Core.DataLayer.DataProviders.{dataProviderInfo.NameSpase}
{{
    public interface I{dataProviderInfo.BusinessObject}Data : IDataProviderBase<{dataProviderInfo.BusinessObject}>
    {{
        {GetMethodsCode(dataProviderInfo)}
    }}
}}";
        }

        public void SaveToFile(string path)
        {

        }

        private string GetMethodsCode(DataProviderInfo dataProviderInfo)
        {
            List<string> resultCodeList = new List<string>();

            if (dataProviderInfo.IsNeedMethodGetList)
                resultCodeList.Add(GetMethodGetListCode(dataProviderInfo));

            if (dataProviderInfo.IsNeedMethodExists)
                resultCodeList.Add(GetMethodExistsCode(dataProviderInfo));

            if (dataProviderInfo.IsNeedMethodDelete)
                resultCodeList.Add(GetMethodDeleteCode(dataProviderInfo));

            if (dataProviderInfo.IsNeedMethodSave)
                resultCodeList.Add(GetMethodSaveCode(dataProviderInfo));

            if (dataProviderInfo.IsNeedMethodGet)
                resultCodeList.Add(GetMethodGetCode(dataProviderInfo));

            if (dataProviderInfo.IsNeedMethodDeleteAll)
                resultCodeList.Add(GetMethodDeleteAllCode(dataProviderInfo));

            if (dataProviderInfo.IsNeedMethodFind)
                resultCodeList.Add(GetMethodFindCode(dataProviderInfo));

            return Join($"{Environment.NewLine}\t\t", resultCodeList);
        }

        private string GetMethodFindCode(DataProviderInfo dataProviderInfo)
        {
            return $@"List<{dataProviderInfo.BusinessObject}> Find(IConnectionManager entry, {Join(", ", dataProviderInfo.Columns.Select(c => c.PropType == "string" 
                    ? ($"{c.PropType} {c.FieldName}, bool {c.FieldName}BeginsWith")
                    : c.PropType + ((c.PropType == "bool" ? "?" : Empty) + $" {c.FieldName}")))}, string orderBy);";
        }

        private string GetMethodDeleteAllCode(DataProviderInfo dataProviderInfo)
        {
            return $@"void DeleteAll(IConnectionManager entry);";
        }

        private string GetMethodGetCode(DataProviderInfo dataProviderInfo)
        {
            return dataProviderInfo.Columns.All(c => !c.IsPK)
                ? "For {Get} method select primary key columns."
                : $@"{dataProviderInfo.BusinessObject} Get(IConnectionManager entry, {Join(", ", dataProviderInfo.Columns.Where(c => c.IsPK).Select(c => $"{c.PropType} {c.FieldName}"))});";
        }

        private string GetMethodSaveCode(DataProviderInfo dataProviderInfo)
        {
            return $@"void Save(IConnectionManager entry, {dataProviderInfo.BusinessObject} {dataProviderInfo.BusinessObjectField});";
        }

        private string GetMethodDeleteCode(DataProviderInfo dataProviderInfo)
        {
            return dataProviderInfo.Columns.All(c => !c.IsPK)
                ? "For {Delete} method select primary key columns."
                : $@"void Delete(IConnectionManager entry, {Join(", ", dataProviderInfo.Columns.Where(c => c.IsPK).Select(c => $"{c.PropType} {c.FieldName}"))});";
        }

        private string GetMethodGetListCode(DataProviderInfo dataProviderInfo)
        {
            return $@"List<{dataProviderInfo.BusinessObject}> GetList(IConnectionManager entry);";
        }

        private string GetMethodExistsCode(DataProviderInfo dataProviderInfo)
        {
            return dataProviderInfo.Columns.All(c => !c.IsPK) 
                ? "For {Exists} method select primary key columns."
                : $@"bool Exists(IConnectionManager entry, {Join(", ", dataProviderInfo.Columns.Where(c => c.IsPK).Select(c => $"{c.PropType} {c.FieldName}"))});";
        }
    }
}
