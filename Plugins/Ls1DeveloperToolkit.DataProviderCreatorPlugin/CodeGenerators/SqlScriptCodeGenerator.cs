﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ls1DeveloperToolkit.DataProviderCreatorPlugin.Interfaces;

namespace Ls1DeveloperToolkit.DataProviderCreatorPlugin.CodeGenerators
{
    public class SqlScriptCodeGenerator : ICodeGenerator
    {
        public SqlScriptCodeGenerator() { }

        public string GenerateCode(DataProviderInfo dataProviderInfo)
        {
            return 
$@"/*
	Date created	: {DateTime.Now.ToString("dd.MM.yyy")}
	Description		: Added {dataProviderInfo.TableName} table 
*/

USE LSPOSNET
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='dbo' and TABLE_NAME = '{dataProviderInfo.TableName}')
BEGIN
	CREATE TABLE [dbo].[{dataProviderInfo.TableName}](
		{GetColumns(dataProviderInfo)}{GetPrimaryKeyConstraint(dataProviderInfo)}
	) ON [PRIMARY]
END
GO
";
        }

        public void SaveToFile(string path)
        {

        }

        private string GetPrimaryKeyConstraint(DataProviderInfo dataProviderInfo)
        {
            return $@",
        CONSTRAINT [PK_{dataProviderInfo.TableName}] PRIMARY KEY CLUSTERED
		(
            {string.Join($",{Environment.NewLine}\t\t\t", dataProviderInfo.Columns.Where(c => c.IsPK).Select(c => $"{c.ColNameWithBrackets} ASC"))}{GetDataAreaIdForPKConstraint(dataProviderInfo)}
		) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]";
        }

        private string GetDataAreaIdForPKConstraint(DataProviderInfo dataProviderInfo)
        {
            return dataProviderInfo.UseDataAreaId
                ? $",{Environment.NewLine}\t\t\t[DATAAREAID] ASC"
                : string.Empty;
        }

        private string GetColumns(DataProviderInfo dataProviderInfo)
        {
            List<string> columnsList = new List<string>();

            dataProviderInfo.Columns.Select(c => $"{c.ColNameWithBrackets} {c.SqlType}{GetDefaultConstraint(dataProviderInfo.TableName, c)}").ToList().ForEach(columnsList.Add);

            if (dataProviderInfo.UseDataAreaId)
                columnsList.Add($"[DATAAREAID] [NVARCHAR](4) NOT NULL CONSTRAINT [{dataProviderInfo.TableName}_DFV_DATAAREAID] DEFAULT ('LSR')");

            return string.Join($",{Environment.NewLine}\t\t", columnsList);
        }

        private string GetDefaultConstraint(string tableName, DataProviderInfo.Column column)
        {
            return column.IsPK
                ? $" NOT NULL CONSTRAINT [{tableName}_DFV_{column.ColName}] DEFAULT {GetDefaultValue(column)}"
                : " NULL";
        }

        private string GetDefaultValue(DataProviderInfo.Column column)
        {
            string sqlType = column.SqlType.ToLower();

            string defaultValue = "''";

            if (sqlType.Contains("varchar"))
                defaultValue = "''";
            else if (sqlType.Contains("int") || sqlType.Contains("float") || sqlType.Contains("numeric") || sqlType.Contains("bit"))
                defaultValue = "0";
            else if (sqlType.Contains("uniqueidentifier"))
                defaultValue = $"'{Guid.Empty.ToString()}'";
            else if (sqlType.Contains("datetime"))
                defaultValue = $"'{DateTime.MinValue.ToString("yyyy-MM-dd")}'";

            return defaultValue;
        }
    }
}
