﻿using Ls1DeveloperToolkit.DataProviderCreatorPlugin.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.String;

namespace Ls1DeveloperToolkit.DataProviderCreatorPlugin.CodeGenerators
{
    public class SqlDataProviderCodeGenerator : ICodeGenerator
    {
        public SqlDataProviderCodeGenerator()
        {
        }

        public string GenerateCode(DataProviderInfo dataProviderInfo)
        {
            return $@"using LSOne.Poresy.Core.DataLayer.BusinessObjects.{dataProviderInfo.NameSpase};
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using LSOne.DataLayer.SqlConnector;
using LSOne.DataLayer.SqlConnector.DataProviders;
using LSOne.DataLayer.GenericConnector.Interfaces;
using LSOne.Poresy.Core.DataLayer.DataProviders.{dataProviderInfo.NameSpase};
using LSOne.Utilities.DataTypes;
using LSOne.DataLayer.BusinessObjects;

namespace LSOne.Poresy.Core.DataLayer.SQLDataProviders.{dataProviderInfo.NameSpase}
{{
    public class {dataProviderInfo.BusinessObject}Data : SqlServerDataProviderBase, I{dataProviderInfo.BusinessObject}Data
    {{
        private const string BaseSql = 
            @""SELECT
                {Join($",{Environment.NewLine}\t\t\t\t", dataProviderInfo.Columns.Select(c => $"t.{c.ColNameWithBrackets} AS {c.ColNameWithBrackets}"))}
            FROM
                {dataProviderInfo.TableNameWithBrackets} t
                "";
            

        private static void Populate(IDataReader dr, {dataProviderInfo.BusinessObject} {dataProviderInfo.BusinessObjectField})
        {{
            {
                Join($"{Environment.NewLine}\t\t\t", dataProviderInfo.Columns.Select(c =>
                    $@"{dataProviderInfo.BusinessObjectField}.{c.PropName} = dr[""{c.ColName}""].MapTo({dataProviderInfo.BusinessObjectField}.{c.PropName});"))
            }
        }}
       
        {GetMethodsCode(dataProviderInfo)}
    }}
}}";
        }

        public void SaveToFile(string path)
        {

        }
        
        private string GetMethodsCode(DataProviderInfo dataProviderInfo)
        {
            List<string> resultCodeList = new List<string>();

            if (dataProviderInfo.IsNeedMethodGetList)
                resultCodeList.Add(GetMethodGetListCode(dataProviderInfo));

            if (dataProviderInfo.IsNeedMethodExists)
                resultCodeList.Add(GetMethodExistsCode(dataProviderInfo));

            if (dataProviderInfo.IsNeedMethodDelete)
                resultCodeList.Add(GetMethodDeleteCode(dataProviderInfo));

            if (dataProviderInfo.IsNeedMethodSave)
                resultCodeList.Add(GetMethodSaveCode(dataProviderInfo));

            if (dataProviderInfo.IsNeedMethodGet)
                resultCodeList.Add(GetMethodGetCode(dataProviderInfo));

            if (dataProviderInfo.IsNeedMethodDeleteAll)
                resultCodeList.Add(GetMethodDeleteAllCode(dataProviderInfo));

            if (dataProviderInfo.IsNeedMethodFind)
                resultCodeList.Add(GetMethodFindCode(dataProviderInfo));

            return Join($"{Environment.NewLine}{Environment.NewLine}\t\t", resultCodeList);
        }

        #region Code generators for DataProvider methods

        private string GetMethodFindCode(DataProviderInfo dataProviderInfo)
        {
            string signature = $@"public List<{dataProviderInfo.BusinessObject}> Find(IConnectionManager entry, {Join(", ", dataProviderInfo.Columns.Select(c => c.PropType == "string"
                ? ($"{c.PropType} {c.FieldName}, bool {c.FieldName}BeginsWith")
                : c.PropType + ((c.PropType == "bool" ? "?" : Empty) + $" {c.FieldName}")
            ))}, string orderBy)";

            return $@"{signature}
        {{
            var result = new List<{dataProviderInfo.BusinessObject}>();

            using (var cmd = entry.Connection.CreateCommand())
            {{
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = @""SELECT * FROM (SELECT 
                {Join($",{Environment.NewLine}\t\t\t\t", dataProviderInfo.Columns.Select(c => $"t.{c.ColNameWithBrackets} AS {c.ColNameWithBrackets}"))}, 
                ROW_NUMBER() OVER(ORDER BY "" + orderBy
                                                            + @"") AS rownum 
                FROM {dataProviderInfo.TableNameWithBrackets} t 
                {GetWhereCondition(dataProviderInfo, false, true)}
                    "";
                
                {GetFindConditions(dataProviderInfo)}
                
                cmd.CommandText += @"") AS QUERY ORDER BY rownum"";
                {GetMakeParams(dataProviderInfo, false)}

                using (var reader = entry.Connection.ExecuteReader(cmd, CommandType.Text))
                {{
                    while (reader.Read())
                    {{
                        {dataProviderInfo.BusinessObject} record = new {dataProviderInfo.BusinessObject}() 
                        {{
                            {
                                Join($",{Environment.NewLine}\t\t\t\t\t\t\t", dataProviderInfo.Columns.Select(c =>
                                    $@"{c.PropName} = ({c.PropType})reader[""{c.ColName}""]"))
                            }
                        }};
                        result.Add(record);
                    }}
                }}

                return result;
            }}
        }}
";
        }

        private string GetMethodDeleteAllCode(DataProviderInfo dataProviderInfo)
        {
            return $@"public void DeleteAll(IConnectionManager entry)
        {{
            using (var cmd = entry.Connection.CreateCommand())
            {{
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = ""delete from {dataProviderInfo.TableName} {GetWhereCondition(dataProviderInfo, false)}"";
                {GetMakeParams(dataProviderInfo, false)}

                cmd.ExecuteNonQuery();
            }}
        }}";
        }

        private string GetMethodGetCode(DataProviderInfo dataProviderInfo)
        {
            string signature = dataProviderInfo.Columns.All(c => !c.IsPK)
                ? "For {Get} method select primary key columns."
                : $@"public {dataProviderInfo.BusinessObject} Get(IConnectionManager entry, {Join(", ", dataProviderInfo.GetPkColumns().Select(c => $"{c.PropType} {c.FieldName}"))})";

            return $@"{signature}
        {{
            using (var cmd = entry.Connection.CreateCommand())
            {{
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = BaseSql + @""{GetWhereCondition(dataProviderInfo, true)}"";

                {GetMakeParams(dataProviderInfo, true)}

                return Execute<{dataProviderInfo.BusinessObject}>(entry, cmd, CommandType.Text, Populate).FirstOrDefault();
            }}
        }}";
        }

        private string GetMethodSaveCode(DataProviderInfo dataProviderInfo)
        {
            string businessObjectVarName = dataProviderInfo.BusinessObjectField;

            return $@"public void Save(IConnectionManager entry, {dataProviderInfo.BusinessObject} {businessObjectVarName})
        {{
            SqlServerStatement statement = new SqlServerStatement(""{dataProviderInfo.TableName}"");
            
            if (!Exists(entry, {string.Join(", ", dataProviderInfo.GetPkColumns().Select(c => $"{businessObjectVarName}.{c.PropName}"))}))
            {{
                statement.StatementType = StatementType.Insert;

                {GetSaveKeys(dataProviderInfo, true)}
            }}
            else
            {{
                statement.StatementType = StatementType.Update;

                {GetSaveKeys(dataProviderInfo, false)}
            }}

            {GetSaveFields(dataProviderInfo)}

            entry.Connection.ExecuteStatement(statement);
        }}
";
        }

        private string GetMethodDeleteCode(DataProviderInfo dataProviderInfo)
        {
            string signature = dataProviderInfo.Columns.All(c => !c.IsPK)
                ? "For {Delete} method select primary key columns."
                : $@"public void Delete(IConnectionManager entry, {Join(", ", dataProviderInfo.GetPkColumns().Select(c => $"{c.PropType} {c.FieldName}"))})";

            List<string> bodyLines = new List<string>();

            bodyLines.Add($"var combinationID = new RecordIdentifier({Join(", ", dataProviderInfo.GetPkColumns().Select(c => $"{c.FieldName}"))});");
            bodyLines.Add($"DeleteRecord(entry, \"{dataProviderInfo.TableName}\", new[] {{ {Join(", ", dataProviderInfo.GetPkColumns().Select(c => $"\"{c.ColName}\""))} }}, combinationID, Permission.Manage{dataProviderInfo.BusinessObject}, {dataProviderInfo.UseDataAreaId.ToString().ToLower()});");

            string body = Join($"{Environment.NewLine}\t\t\t", bodyLines);

            return $@"{signature}
        {{
            {body}
        }}";
        }

        private string GetMethodGetListCode(DataProviderInfo dataProviderInfo)
        {
            return $@"public List<{dataProviderInfo.BusinessObject}> GetList(IConnectionManager entry)
        {{
            using (var cmd = entry.Connection.CreateCommand())
            {{
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = BaseSql + @""{GetWhereCondition(dataProviderInfo, false)}"";
                {GetMakeParams(dataProviderInfo, false)}

                return Execute<{dataProviderInfo.BusinessObject}>(entry, cmd, CommandType.Text, Populate);
            }}
        }}";
        }

        private string GetMethodExistsCode(DataProviderInfo dataProviderInfo)
        {
            string signature = dataProviderInfo.Columns.All(c => !c.IsPK)
                ? "For {Exists} method select primary key columns."
                : $@"public bool Exists(IConnectionManager entry, {Join(", ", dataProviderInfo.GetPkColumns().Select(c => $"{c.PropType} {c.FieldName}"))})";

            List<string> bodyLines = new List<string>();

            bodyLines.Add($"var combinationID = new RecordIdentifier({Join(", ", dataProviderInfo.GetPkColumns().Select(c => $"{c.FieldName}"))});");
            bodyLines.Add($"return RecordExists(entry, \"{dataProviderInfo.TableName}\", new[] {{ {Join(", ", dataProviderInfo.GetPkColumns().Select(c => $"\"{c.ColName}\""))} }}, combinationID);");

            string body = Join($"{Environment.NewLine}\t\t\t", bodyLines);

            return $@"{signature}
        {{
            {body}
        }}";
        }

        #endregion

        #region Help methods

        private string GetFindConditions(DataProviderInfo dataProviderInfo)
        {
            List<string> conditions = new List<string>();

            foreach (DataProviderInfo.Column column in dataProviderInfo.Columns)
            {

                conditions.Add(column.PropType == "string"
                    ? $@"if ({column.FieldName} != string.Empty)
                {{
                    string where = {column.FieldName}BeginsWith ? "" AND t.{column.ColNameWithBrackets} LIKE @{
                            column.FieldName
                        } + '%'""
                        : "" AND t.{column.ColNameWithBrackets} LIKE '%' + @{column.FieldName} + '%'"";
                    cmd.CommandText += where;
                    MakeParam(cmd, ""{column.FieldName}"", {column.FieldName}, {GetSqlDbType(column)});
                }}"
                    : $@"if ({column.FieldName} != {GetNotSettedValueForType(column.PropType)})
                {{
                    string where = "" AND t.{column.ColName} = @{column.FieldName} "";
                    cmd.CommandText += where;
                    MakeParam(cmd, ""{column.FieldName}"", {column.FieldName}, {GetSqlDbType(column)});
                }}");
    }

            return Join($"{Environment.NewLine}{Environment.NewLine}\t\t\t\t", conditions);
        }

        private string GetNotSettedValueForType(string type)
        {
            string notSettedValue = "null";

            if (type == "string")
                notSettedValue = "string.Empty";
            else if (type == "int")
                notSettedValue = "-1";
            else if (type == "Guid")
                notSettedValue = "Guid.Empty";
            else if (type == "double")
                notSettedValue = "-1";
            else if (type == "DateTime")
                notSettedValue = "DateTime.MinValue";
            else if (type == "bool")
                notSettedValue = "null";
            
            return notSettedValue;
        }

        private string GetWhereCondition(DataProviderInfo dataProviderInfo, bool addPkColumns, bool emptyWhereIfNoConditions = false)
        {
            List<string> conditions = new List<string>();

            if (addPkColumns)
                dataProviderInfo.GetPkColumns().Select(c => $"t.{c.ColNameWithBrackets} = @{c.PropName}").ToList()
                    .ForEach(conditions.Add);

            if (dataProviderInfo.UseDataAreaId)
                conditions.Add("t.[DATAAREAID] = @DataAreaID");

            return conditions.Any()
                ? $"WHERE {Join($"{Environment.NewLine}\t\t\t\t\tAND ", conditions)}"
                : (emptyWhereIfNoConditions ? "WHERE " : Empty);
        }

        private string GetSaveFields(DataProviderInfo dataProviderInfo)
        {
            return Join($"{Environment.NewLine}\t\t\t", dataProviderInfo.GetNotPkColumns().Select(c => $"statement.AddField(\"{c.ColName}\", {dataProviderInfo.BusinessObjectField}.{c.PropName}, {GetSqlDbType(c)});"));
        }

        private string GetSaveKeys(DataProviderInfo dataProviderInfo, bool isInsert)
        {
            string addMethodName = isInsert ? "AddKey" : "AddCondition";

            string columns = Join($"{Environment.NewLine}\t\t\t\t", dataProviderInfo.GetPkColumns().Select(c => $"statement.{addMethodName}(\"{c.ColName}\", {dataProviderInfo.BusinessObjectField}.{c.PropName}, {GetSqlDbType(c)});"));
            if (dataProviderInfo.UseDataAreaId)
                columns += $"{Environment.NewLine}\t\t\t\tstatement.AddKey(\"DATAAREAID\", entry.Connection.DataAreaId);";

            return columns;
        }

        private string GetMakeParams(DataProviderInfo dataProviderInfo, bool addPkColumns)
        {
            List<string> makeParams = new List<string>();

            if (addPkColumns)
                dataProviderInfo.GetPkColumns().Select(c => $"MakeParam(cmd, \"{c.PropName}\", {c.FieldName}, {GetSqlDbType(c)});").ToList().ForEach(makeParams.Add);

            if (dataProviderInfo.UseDataAreaId)
                makeParams.Add("MakeParam(cmd, \"DataAreaID\", entry.Connection.DataAreaId);");

            return makeParams.Any()
                ? Join($"{Environment.NewLine}\t\t\t\t", makeParams)
                : Empty;
        }

        private string GetSqlDbType(DataProviderInfo.Column column)
        {
            string sqlDbType = "SqlDbType.NVarChar";
            string type = column.PropType;

            if (type == "string")
                sqlDbType = "SqlDbType.NVarChar";
            else if (type == "int")
                sqlDbType = "SqlDbType.Int";
            else if (type == "Guid")
                sqlDbType = "SqlDbType.UniqueIdentifier";
            else if (type == "double")
                sqlDbType = "SqlDbType.Float";
            else if (type == "DateTime")
                sqlDbType = "SqlDbType.DateTime";
            else if (type == "bool")
                sqlDbType = "SqlDbType.Bit";

            return sqlDbType;
        }

        #endregion
    }
}
