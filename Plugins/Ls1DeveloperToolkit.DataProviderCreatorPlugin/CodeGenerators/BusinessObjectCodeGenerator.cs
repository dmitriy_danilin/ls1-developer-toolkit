﻿using Ls1DeveloperToolkit.DataProviderCreatorPlugin.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ls1DeveloperToolkit.DataProviderCreatorPlugin.CodeGenerators
{
    public class BusinessObjectCodeGenerator : ICodeGenerator
    {
        public BusinessObjectCodeGenerator()
        {
        }

        public string GenerateCode(DataProviderInfo dataProviderInfo)
        {
            return $@"using System;
using LSOne.DataLayer.BusinessObjects;

namespace LSOne.Poresy.Core.DataLayer.BusinessObjects.{dataProviderInfo.NameSpase}
{{
    public class {dataProviderInfo.BusinessObject} : DataEntity
    {{
        {string.Join($"{Environment.NewLine}\t\t", dataProviderInfo.Columns.Select(p => $"public {p.PropType} {p.PropName} {{ get; set; }}"))}
    }}
}}";
        }

        public void SaveToFile(string path)
        {

        }
    }
}
