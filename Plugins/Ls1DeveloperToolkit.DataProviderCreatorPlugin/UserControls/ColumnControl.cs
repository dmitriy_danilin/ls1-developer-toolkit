﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.String;

namespace Ls1DeveloperToolkit.DataProviderCreatorPlugin.UserControls
{
    public partial class ColumnControl : UserControl
    {
        public int Id { get; set; }

        public bool IsPrimaryKey => chkbPK.Checked;
        public string SqlColumnName => txtColName.Text;
        public string SqlType => cmbSqlType.Text;
        public string PropertyName => txtPropName.Text;
        public string PropertyType => cmbPropType.Text;

        public event EventHandler BtnDelete_Click;
        public event EventHandler ControlValueChanged;

        public ColumnControl()
        {
            InitializeComponent();
        }

        public bool ValidateControls()
        {
            return !IsNullOrEmpty(txtColName.Text) &&
                    !IsNullOrEmpty(cmbSqlType.Text) &&
                    !IsNullOrEmpty(cmbPropType.Text) &&
                    !IsNullOrEmpty(txtPropName.Text);
        }

        private void BtnDel_Click(object sender, EventArgs e)
        {
            BtnDelete_Click?.Invoke(this, e);
        }

        private void Control_ValueChanged(object sender, EventArgs e)
        {
            ControlValueChanged?.Invoke(this, e);
        }

        private void txtColName_Leave(object sender, EventArgs e)
        {
            if (IsNullOrWhiteSpace(txtPropName.Text) && !IsNullOrWhiteSpace(txtColName.Text))
            {
                txtPropName.Text = txtColName.Text.ToLower().FirstCharUpper();
            }
        }

        private void cmbSqlType_Leave(object sender, EventArgs e)
        {
            if (!IsNullOrWhiteSpace(cmbSqlType.Text) && IsNullOrWhiteSpace(cmbPropType.Text))
            {
                string sqlType = cmbSqlType.Text;

                if (sqlType.Contains("varchar"))
                    cmbPropType.Text = "string";
                else if (sqlType.Contains("int"))
                    cmbPropType.Text = "int";
                else if (sqlType.Contains("uniqueidentifier"))
                    cmbPropType.Text = "Guid";
                else if (sqlType.Contains("float") || sqlType.Contains("numeric"))
                    cmbPropType.Text = "double";
                else if (sqlType.Contains("datetime"))
                    cmbPropType.Text = "DateTime";
                else if (sqlType.Contains("bit"))
                    cmbPropType.Text = "bool";
            }
        }
    }
}
