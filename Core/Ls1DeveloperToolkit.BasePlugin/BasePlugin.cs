﻿using System.Windows.Forms;

using Ls1DeveloperToolkit.Interfaces;

namespace Ls1DeveloperToolkit.BasePlugin
{
    public abstract class BasePlugin : IPlugin
    {
        /// <summary>
        /// Plugin visible name
        /// </summary>
        public abstract string PluginVisibleName { get; }
        
        /// <summary>
        /// Main plugin page
        /// </summary>
        public abstract UserControl PluginPage { get; }

        /// <summary>
        /// Load plugin tab page method
        /// </summary>
        /// <param name="tabControl">TabControl oject to embed tab</param>
        public virtual void LoadPlugin(TabControl tabControl)
        {
            var tabPage = CreatePluginTabPage(PluginPage);

            tabControl.TabPages.Add(tabPage);
        }

        private TabPage CreatePluginTabPage(UserControl pluginPage)
        {
            pluginPage.Dock = DockStyle.Fill;

            var tabPage = new TabPage { Text = PluginVisibleName };
            tabPage.Controls.Add(pluginPage);

            return tabPage;
        }
    }
}
