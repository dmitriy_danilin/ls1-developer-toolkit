﻿using System;
using System.Collections.Generic;
using System.IO;

using Ninject;
using Ninject.Extensions.Conventions;

using Ls1DeveloperToolkit.Interfaces;

namespace Ls1DeveloperToolkit
{
    public class PluginManager : IDisposable
    {
        private readonly IKernel _kernel;

        public PluginManager()
        {
            _kernel = new StandardKernel();
        }

        private IEnumerable<IPlugin> GetPluginsByName(string name)
        {
            _kernel.Bind(x =>
                {
                    x.FromAssembliesInPath(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Plugins"))
                        .SelectAllClasses()
                        .InheritedFrom<IPlugin>()
                        .BindAllInterfaces();
                }
            );

            return _kernel.GetAll<IPlugin>();
        }

        public IEnumerable<IPlugin> GetPlugins()
        {
            return GetPlugins(string.Empty);
        }

        public IEnumerable<IPlugin> GetPlugins(string pluginName)
        {
            return GetPluginsByName(pluginName);
        }

        public void Dispose()
        {
            _kernel.Dispose();
        }
    }
}
